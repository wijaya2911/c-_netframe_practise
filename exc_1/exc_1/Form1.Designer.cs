﻿namespace exc_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LstboxDisplay = new System.Windows.Forms.ListBox();
            this.CmbboxDisplay = new System.Windows.Forms.ComboBox();
            this.BtnMoveToLstbox = new System.Windows.Forms.Button();
            this.BtnMoveToCmbbox = new System.Windows.Forms.Button();
            this.BtnShowSelected = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LstboxDisplay
            // 
            this.LstboxDisplay.FormattingEnabled = true;
            this.LstboxDisplay.Location = new System.Drawing.Point(34, 32);
            this.LstboxDisplay.Name = "LstboxDisplay";
            this.LstboxDisplay.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.LstboxDisplay.Size = new System.Drawing.Size(268, 394);
            this.LstboxDisplay.TabIndex = 0;
            this.LstboxDisplay.SelectedIndexChanged += new System.EventHandler(this.LstboxDisplay_SelectedIndexChanged);
            // 
            // CmbboxDisplay
            // 
            this.CmbboxDisplay.FormattingEnabled = true;
            this.CmbboxDisplay.Location = new System.Drawing.Point(449, 143);
            this.CmbboxDisplay.Name = "CmbboxDisplay";
            this.CmbboxDisplay.Size = new System.Drawing.Size(214, 21);
            this.CmbboxDisplay.TabIndex = 1;
            this.CmbboxDisplay.SelectedIndexChanged += new System.EventHandler(this.CmbboxDisplay_SelectedIndexChanged);
            // 
            // BtnMoveToLstbox
            // 
            this.BtnMoveToLstbox.Enabled = false;
            this.BtnMoveToLstbox.Location = new System.Drawing.Point(337, 191);
            this.BtnMoveToLstbox.Name = "BtnMoveToLstbox";
            this.BtnMoveToLstbox.Size = new System.Drawing.Size(79, 27);
            this.BtnMoveToLstbox.TabIndex = 2;
            this.BtnMoveToLstbox.Text = "<-";
            this.BtnMoveToLstbox.UseVisualStyleBackColor = true;
            this.BtnMoveToLstbox.Click += new System.EventHandler(this.BtnMoveToLstbox_Click);
            // 
            // BtnMoveToCmbbox
            // 
            this.BtnMoveToCmbbox.Enabled = false;
            this.BtnMoveToCmbbox.Location = new System.Drawing.Point(337, 254);
            this.BtnMoveToCmbbox.Name = "BtnMoveToCmbbox";
            this.BtnMoveToCmbbox.Size = new System.Drawing.Size(79, 27);
            this.BtnMoveToCmbbox.TabIndex = 3;
            this.BtnMoveToCmbbox.Text = "->";
            this.BtnMoveToCmbbox.UseVisualStyleBackColor = true;
            this.BtnMoveToCmbbox.Click += new System.EventHandler(this.BtnMoveToCmbbox_Click);
            // 
            // BtnShowSelected
            // 
            this.BtnShowSelected.Enabled = false;
            this.BtnShowSelected.Location = new System.Drawing.Point(337, 496);
            this.BtnShowSelected.Name = "BtnShowSelected";
            this.BtnShowSelected.Size = new System.Drawing.Size(79, 27);
            this.BtnShowSelected.TabIndex = 4;
            this.BtnShowSelected.Text = "Display";
            this.BtnShowSelected.UseVisualStyleBackColor = true;
            this.BtnShowSelected.Click += new System.EventHandler(this.BtnShowSelected_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 632);
            this.Controls.Add(this.BtnShowSelected);
            this.Controls.Add(this.BtnMoveToCmbbox);
            this.Controls.Add(this.BtnMoveToLstbox);
            this.Controls.Add(this.CmbboxDisplay);
            this.Controls.Add(this.LstboxDisplay);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LstboxDisplay;
        private System.Windows.Forms.ComboBox CmbboxDisplay;
        private System.Windows.Forms.Button BtnMoveToLstbox;
        private System.Windows.Forms.Button BtnMoveToCmbbox;
        private System.Windows.Forms.Button BtnShowSelected;
    }
}

