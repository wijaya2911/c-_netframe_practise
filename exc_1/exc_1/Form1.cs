﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exc_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<String> LstbxItems = new List<string>();
        List<String> CmbbxItems = new List<string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            //populate the listbox and combobox and deacativate the buttons
            LstbxItems.Add("data 1");
            LstbxItems.Add("data 2");
            LstbxItems.Add("data 3");
            LstbxItems.Add("data 4");
            LstbxItems.Add("data 5");

            CmbbxItems.Add("data 6");
            CmbbxItems.Add("data 7");
            CmbbxItems.Add("data 8");
            CmbbxItems.Add("data 9");

            foreach (String data1 in LstbxItems)
            {
                LstboxDisplay.Items.Add(data1);
            }

            foreach (String data2 in CmbbxItems)
            {
                CmbboxDisplay.Items.Add(data2);
            }
        }

        //check if any option is selected
        //if yes activate corresponding buttons
        private void CheckSelected()
        {
            int Cmbbox = CmbboxDisplay.SelectedIndex != -1 ? 1 : 0;
            int Lstbox = LstboxDisplay.SelectedIndex != -1 ? 1 : 0;

            int Total = Cmbbox + Lstbox;

            if (Cmbbox == 1)
            {
                BtnMoveToLstbox.Enabled = true;
            }
            else
            {
                BtnMoveToLstbox.Enabled = false;
            }

            if (Lstbox == 1)
            {
                BtnMoveToCmbbox.Enabled = true;
            }
            else
            {
                BtnMoveToCmbbox.Enabled = false;
            }

            if (Total >0)
            {
                BtnShowSelected.Enabled = true;
            }
            else
            {
                BtnShowSelected.Enabled = false;
            }
        }

        private void LstboxDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSelected();
        }

        private void CmbboxDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSelected();
        }

        //button to move selected listbox items to combobox
        private void BtnMoveToLstbox_Click(object sender, EventArgs e)
        {
            LstboxDisplay.Items.Add(CmbboxDisplay.Items[CmbboxDisplay.SelectedIndex]);
            CmbboxDisplay.Items.RemoveAt(CmbboxDisplay.SelectedIndex);
            CmbboxDisplay.SelectedIndex = -1;
            BtnMoveToLstbox.Enabled = false;
            BtnShowSelected.Enabled = false;
        }

        //button to move selected combobox items to listbox
        private void BtnMoveToCmbbox_Click(object sender, EventArgs e)
        {
            int count = LstboxDisplay.SelectedIndices.Count;
            for (int i =  0; i < count; i++)
            {
                CmbboxDisplay.Items.Add(LstboxDisplay.Items[LstboxDisplay.SelectedIndices[0]]);
                LstboxDisplay.Items.RemoveAt(LstboxDisplay.SelectedIndices[0]);
            }
            BtnMoveToCmbbox.Enabled = false;
            BtnShowSelected.Enabled = false;
        }

        //button to display the selected items in a messagebox
        private void BtnShowSelected_Click(object sender, EventArgs e)
        {
            //items are stored in a list for sorting before displaying
            List<String> result = new List<string>();
            string combine = string.Empty;
            int counter = 0;

            if(LstboxDisplay.SelectedIndex != -1)
            {
                int count = LstboxDisplay.SelectedIndices.Count;
                for (int i = 0; i < count; i++)
                {
                    result.Add(LstbxItems[LstboxDisplay.SelectedIndices[i]].ToString());
                    counter++;
                }
            }

            if(CmbboxDisplay.SelectedIndex !=-1)
            {
                result.Add(CmbbxItems[CmbboxDisplay.SelectedIndex].ToString());
                counter++;
            }

            result.Sort();
            combine = combine + result[0];
            for(int i = 1; i<counter; i++)
            {
                combine = combine + ", " + result[i];
            }

            MessageBox.Show("You have selected: " + combine, "Selection");
        }
    }
}
